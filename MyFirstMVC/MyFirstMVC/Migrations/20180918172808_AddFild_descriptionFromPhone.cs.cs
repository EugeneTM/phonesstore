﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MyFirstMVC.Migrations
{
    public partial class AddFild_descriptionFromPhonecs : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "_description",
                table: "Phones",
                nullable: true);

            migrationBuilder.UpdateData(
                table: "Phones",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Name", "_description" },
                values: new object[] { "iPhone", "Apple iPhone Xs Max 256 Gb (RAM 4 Gb) Dual Sim space gray" });

            migrationBuilder.UpdateData(
                table: "Phones",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "Name", "_description" },
                values: new object[] { "Samsung", "Samsung Galaxy S9 G960F 64 Gb (RAM 4 Gb) Dual Sim midnight black" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "_description",
                table: "Phones");

            migrationBuilder.UpdateData(
                table: "Phones",
                keyColumn: "Id",
                keyValue: 1,
                column: "Name",
                value: "Apple iPhone Xs Max 256 Gb (RAM 4 Gb) Dual Sim space gray");

            migrationBuilder.UpdateData(
                table: "Phones",
                keyColumn: "Id",
                keyValue: 2,
                column: "Name",
                value: "Samsung Galaxy S9 G960F 64 Gb (RAM 4 Gb) Dual Sim midnight black");
        }
    }
}
