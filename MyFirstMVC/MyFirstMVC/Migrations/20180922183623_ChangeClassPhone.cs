﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MyFirstMVC.Migrations
{
    public partial class ChangeClassPhone : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Phones_ExchangeRates_ExchangeKGSId",
                table: "Phones");

            migrationBuilder.DropForeignKey(
                name: "FK_Phones_ExchangeRates_ExchangeRUBId",
                table: "Phones");

            migrationBuilder.DropIndex(
                name: "IX_Phones_ExchangeKGSId",
                table: "Phones");

            migrationBuilder.DropIndex(
                name: "IX_Phones_ExchangeRUBId",
                table: "Phones");

            migrationBuilder.DropColumn(
                name: "ExchangeKGSId",
                table: "Phones");

            migrationBuilder.DropColumn(
                name: "ExchangeRUBId",
                table: "Phones");

            migrationBuilder.DropColumn(
                name: "_priceKGS",
                table: "Phones");

            migrationBuilder.DropColumn(
                name: "_priceRUB",
                table: "Phones");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "ExchangeKGSId",
                table: "Phones",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "ExchangeRUBId",
                table: "Phones",
                nullable: true);

            migrationBuilder.AddColumn<double>(
                name: "_priceKGS",
                table: "Phones",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddColumn<double>(
                name: "_priceRUB",
                table: "Phones",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.UpdateData(
                table: "Phones",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "_priceKGS", "_priceRUB" },
                values: new object[] { 2.0, 1.0 });

            migrationBuilder.UpdateData(
                table: "Phones",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "_priceKGS", "_priceRUB" },
                values: new object[] { 2.0, 1.0 });

            migrationBuilder.CreateIndex(
                name: "IX_Phones_ExchangeKGSId",
                table: "Phones",
                column: "ExchangeKGSId");

            migrationBuilder.CreateIndex(
                name: "IX_Phones_ExchangeRUBId",
                table: "Phones",
                column: "ExchangeRUBId");

            migrationBuilder.AddForeignKey(
                name: "FK_Phones_ExchangeRates_ExchangeKGSId",
                table: "Phones",
                column: "ExchangeKGSId",
                principalTable: "ExchangeRates",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Phones_ExchangeRates_ExchangeRUBId",
                table: "Phones",
                column: "ExchangeRUBId",
                principalTable: "ExchangeRates",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
