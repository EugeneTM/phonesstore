﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MyFirstMVC.Models
{
    public class Phone
    {
        public int Id { get; set; }
        [Display(Name = "Телефон")] public string Name { get; set; }
        public string Company { get; set; }

        [Display(Name = "Цена")]
        [DisplayFormat(DataFormatString = "{0:C0}", ApplyFormatInEditMode = true)]
        public double Price { get; set; }

        [Display(Name = "Описание")] public string _description { get; set; }
        public string _redirectUrl { get; set; }

        public int CategoryId { get; set; }
        public Category Category { get; set; }

        public IEnumerable<PhoneOnStock> PhoneOnStocks { get; set; }

        public IEnumerable<Order> Orders { get; set; }
    }
}