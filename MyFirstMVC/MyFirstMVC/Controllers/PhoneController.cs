using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using MyFirstMVC.Models;

namespace MyFirstMVC.Controllers
{
    public class PhoneController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly IHostingEnvironment _environment;

        public PhoneController(ApplicationDbContext context, IHostingEnvironment environment)
        {
            _context = context;
            _environment = environment;
        }

        // GET
        public IActionResult Index()
        {
            CurrencyMethod(out var exchangeRateRub, out var exchangeRateKgs, out var exchangeRateEur);

            IEnumerable<Phone> phone = _context.Phones.OrderBy(p => p.Name);
            return View(phone);
        }

        public void CurrencyMethod(out ExchangeRate exchangeRateRub, out ExchangeRate exchangeRateKgs, out ExchangeRate exchangeRateEur)
        {
            exchangeRateRub = _context.ExchangeRates.FirstOrDefault(x => x._currencyCode == "RUB");
            if (exchangeRateRub != null) ViewBag.RateRub = exchangeRateRub._currencyRate;

            exchangeRateKgs = _context.ExchangeRates.FirstOrDefault(x => x._currencyCode == "KGS");
            if (exchangeRateKgs != null) ViewBag.RateKgs = exchangeRateKgs._currencyRate;

            exchangeRateEur = _context.ExchangeRates.FirstOrDefault(x => x._currencyCode == "EUR");
            if (exchangeRateEur != null) ViewBag.RateEur = exchangeRateEur._currencyRate;
        }

        // GET: Stock/Phone/5
        public ActionResult Details(int id)
        {
            Phone phone = _context.Phones.FirstOrDefault(p => p.Id == id);
            if (phone == null)
            {
                return NotFound($"Телефон не найден {id}");
            }

            return View(phone);
        }


        public IActionResult Download(string id)
        {
            try
            {
                string filePath = Path.Combine(_environment.ContentRootPath, $"Files/phone_{id}.pdf");
                string fileType = "application/pdf";
                string fileName = $"phone_{id}.pdf";
                if (!System.IO.File.Exists(filePath))
                {
                    throw new FileNotFoundException($"Телефон {id} не найдена");
                }

                return PhysicalFile(filePath, fileType, fileName);
            }
            catch (Exception e)
            {
                ViewData["Message"] = e.Message;
                return View("404");
            }
        }

        public IActionResult Create()
        {
            throw new NotImplementedException();
        }

        public IActionResult Edit()
        {
            throw new NotImplementedException();
        }

        public IActionResult Delete()
        {
            throw new NotImplementedException();
        }


        public IActionResult Info(int id)
        {
            Phone phone = _context.Phones.FirstOrDefault(p => p.Id == id);
            return RedirectPermanent(phone._redirectUrl);
        }
    }
}